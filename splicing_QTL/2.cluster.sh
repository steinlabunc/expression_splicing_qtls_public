#!/bin/bash
#SBATCH -n 1
#SBATCH --mem=10g
#SBATCH -t 18:00:00

module add samtools;
module add python/2.7.12;

#python /proj/steinlab/projects/R00/eQTLanalysis/Emmaxresults/QCresults/sQTLs/leafcutter/scripts/leafcutter_cluster_regtools.py -j ../pro.juncfiles.txt -m 50 -o progenitor -l 500000

#python /proj/steinlab/projects/R00/eQTLanalysis/Emmaxresults/QCresults/sQTLs/leafcutter/scripts/leafcutter_cluster_regtools.py -j ../neuro.juncfiles.txt -m 50 -o neuron -l 500000

python /proj/steinlab/projects/R00/eQTLanalysis/Emmaxresults/QCresults/sQTLs/leafcutter/scripts/leafcutter_cluster_regtools.py -j ../bulkk.juncfiles.txt -m 50 -o bulk -l 500000
