#!/bin/bash
#SBATCH -n 1
#SBATCH -p steinlab
#SBATCH --mem=10g
#SBATCH -t 8:00:00

module add samtools;
module add python/2.7.12;

bamfile=$1;

#FR read-forward/reverse for PE sequencing
dir="/proj/steinlab/projects/R00/eQTLanalysis/Splicing_QTL/regtools/build/"
filedir="/proj/steinlab/projects/R00/eQTLanalysis/ASE/WASP/bams/"
outdir="/proj/steinlab/projects/R00/eQTLanalysis/Splicing_QTL/"
extension="totalRNAseq.duplicateMarked.Aligned.sortedByCoord.sort.bam"

${dir}regtools junctions extract -a 6 \
                                 -m 50 \
                                 -M 500000 \
                                 -s 2 ${filedir}${bamfile}.${extension} \
                                 -o ${outdir}${bamfile}.junc


