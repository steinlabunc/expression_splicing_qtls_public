#!/bin/bash
#SBATCH -n 1
#SBATCH --mem=10g
#SBATCH -t 8:00:00

module add samtools;
module add python/2.7.12;

dir="/proj/steinlab/projects/R00/eQTLanalysis/Emmaxresults/QCresults/sQTLs/leafcutter/scripts/"
python ${dir}prepare_phenotype_table.py ../bulk_perind.counts.gz -p 10
python ${dir}prepare_phenotype_table.py ../progenitor_perind.counts.gz -p 10
python ${dir}prepare_phenotype_table.py ../neuron_perind.counts.gz -p 10
