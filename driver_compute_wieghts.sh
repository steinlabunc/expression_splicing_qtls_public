#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=15g
#SBATCH -t 4-00:00:00

module add plink;
module add gcta;
module add emmax;
module add r/3.6.0;
i=$1

file="/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/progenesTruncated${i}.txt"

while read -r chr gene val1 val2 comb
do
cp /proj/steinlab/projects/R00/eQTLanalysis/TWAS/truncated_Genotype/cell/progenitor/chr${chr}/P-chr${chr}-${gene}* /pine/scr/n/i/nil1/TWAS/tmp/
rm /pine/scr/n/i/nil1/TWAS/tmp/P-chr${chr}-${gene}.log

#Rscript FUSION.compute_weights_updated.R --bfile /proj/steinlab/projects/R00/eQTLanalysis/genofiles/Truncatedgenotypes/withrsIDs/progenitor/chr$chr/$gene --pheno /proj/steinlab/projects/R00/eQTLanalysis/phenofiles/vstNorm/$gene.txt --crossval 85  --kinship /proj/steinlab/projects/R00/eQTLanalysis/genofiles/snpsprowo${i}.hBN.kinf --tmp /proj/steinlab/projects/R00/eQTLanalysis/Emmaxresults/QCresults/TWAS/fusion_twas-master/examples/tmp/$gene --out /proj/steinlab/projects/R00/eQTLanalysis/Emmaxresults/QCresults/TWAS/fusion_twas-master/examples/out/$gene --verbose 0 --save_hsq --models lasso,top1,enet

Rscript FUSION.compute_weights_updatedP.R --bfile truncated_Genotype/cell/progenitor/chr${chr}/P-chr${chr}-$gene \
                                         --crossval 85 \
                                         --covar /proj/steinlab/projects/R00/eQTLanalysis/genofiles/covariates/pro.lasso.txt \
                                         --kinship /proj/steinlab/projects/R00/eQTLanalysis/genofiles/snpsprowo${i}.hBN.kinf \
                                         --tmp /pine/scr/n/i/nil1/TWAS/tmp/P-chr${chr}-$gene \
                                         --out /pine/scr/n/i/nil1/TWAS/out/P-chr${chr}-$gene \
                                         --verbose 0 \
                                         --save_hsq \
                                         --chr ${chr} \
                                         --models lasso,enet,emmax


done<$file

