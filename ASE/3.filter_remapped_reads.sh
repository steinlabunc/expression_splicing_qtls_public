#!/bin/bash
#SBATCH -n 1
#SBATCH -p general
#SBATCH --mem=35g
#SBATCH -t 3-00:00:00


module add wasp
module add python/3.6.6

bamfile=$1;
#cp /proj/steinlab/projects/R00/r00-mrna-seq/results/bams/novaseq/${bamfile}.duplicateMarked.Aligned.sortedByCoord* /pine/scr/n/i/nil1/sQTL/bam/

python3 /nas/longleaf/apps/wasp/2018-07/WASP/mapping/filter_remapped_reads.py \
       find_intersecting_snps/${bamfile}.to.remap.bam \
       remap/${bamfile}.remap.sort.bam \
       filter_remapped_reads/${bamfile}.keep.bam
