#!/bin/bash
#SBATCH -n 1
#SBATCH -p steinlab
#SBATCH --mem=20g
#SBATCH -t 3-00:00:00


module add wasp
module add python/3.6.6

bamfile=$1;
#cp /proj/steinlab/projects/R00/r00-mrna-seq/results/bams/novaseq/${bamfile}.duplicateMarked.Aligned.sortedByCoord* /pine/scr/n/i/nil1/sQTL/bam/
cp /proj/steinlab/projects/FetalTissueQTL/merged_bams/cw/${bamfile}.totalRNAseq.duplicateMarked.Aligned.sortedByCoord* /pine/scr/n/i/nil1/sQTL/bam/
python3 /nas/longleaf/apps/wasp/2018-07/WASP/mapping/find_intersecting_snps.py \
          --is_paired_end \
          --is_sorted \
          --output_dir find_intersecting_snps \
          --snp_tab snp_tab.h5 \
          --snp_index snp_index.h5 \
          --haplotype haplotypes.h5 \
          --samples /proj/steinlab/projects/R00/eQTLanalysis/ASE/bulk.samples.txt \
          /pine/scr/n/i/nil1/sQTL/bam/${bamfile}.totalRNAseq.duplicateMarked.Aligned.sortedByCoord.bam
