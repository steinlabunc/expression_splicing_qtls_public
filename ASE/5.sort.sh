#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=50g
#SBATCH -t 3-00:00:00


module add star/2.5.3a
module add samtools

#genomeDir="/proj/steinlab/projects/R00/r00-mrna-seq/data/reference_genome/GRCh38_release_86/STAR_index_GRCh38"
datadir="/pine/scr/n/i/nil1/WASP/find_intersecting_snps/"
outdir="/pine/scr/n/i/nil1/WASP/remap/"

rna=$1;

samtools sort -o ${outdir}/${rna}.sort.bam ${outdir}/${rna}Aligned.out.bam;
samtools index ${outdir}/${rna}.sort.bam;
