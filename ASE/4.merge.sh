#!/bin/bash
#SBATCH -p steinlab
#SBATCH -n 1
#SBATCH --mem=35g
#SBATCH -t 35:00:00

#module add star/2.5.3a
module add samtools
module add wasp
module add python/3.6.6

rna=$1;

#samtools sort -o ${outdir}/${rna}.sort.bam ${outdir}/${rna}Aligned.out.bam;
#samtools index ${outdir}/${rna}.sort.bam;

samtools merge merge/${rna}.keep.merge.bam \
              filter_remapped_reads/${rna}.keep.bam  \
              find_intersecting_snps/${rna}.keep.bam
samtools sort -o  merge/${rna}.keep.merge.sort.bam \
              merge/${rna}.keep.merge.bam
samtools index merge/${rna}.keep.merge.sort.bam
