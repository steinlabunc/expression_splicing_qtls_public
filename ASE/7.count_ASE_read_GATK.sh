
i=$1;
#i=${i}.bam;
module add gatk/4.0.3.0;
module add picard;
module add samtools;

#java -jar /nas/longleaf/apps/picard/2.20.0/picard-2.20.0/picard.jar CreateSequenceDictionary R= /pine/scr/n/i/nil1/grch38_snp/genome_snp.fa O= /pine/scr/n/i/nil1/grch38_snp/genome_snp.dict

#java -jar /nas/longleaf/apps/picard/2.18.22/picard-2.18.22/picard.jar AddOrReplaceReadGroups I=${dir}/${i}.duplicateMarked.Aligned.sortedByCoord.sort.bam O=${dir}/${i}.duplicateMarked.Aligned.sortedByCoord.groups.sort.bam RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=$i;

#samtools index ${i}.removed_duplicates_groups.bam;

dir="/proj/steinlab/projects/R00/eQTLanalysis/ASE/WASP/bams/"
vcf="/proj/steinlab/projects/R00/atac-qtl/AlleleSpeCaQTLs/VCFWithOnlyBiallelicSNPs/"

#java -jar /nas/longleaf/apps/picard/2.18.22/picard-2.18.22/picard.jar AddOrReplaceReadGroups I=${dir}/${i}.duplicateMarked.Aligned.sortedByCoord.sort.bam O=${dir}/${i}.duplicateMarked.Aligned.sortedByCoord.groups.sort.bam RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=$i;

gatk ASEReadCounter --input ${dir}/${i}.duplicateMarked.Aligned.sortedByCoord.groups.sort.bam \
                    --variant ${vcf}/1.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/2.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/3.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/4.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/5.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/6.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/7.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/8.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/9.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/10.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/11.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/12.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/13.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/14.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/15.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/16.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/17.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/18.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/19.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/20.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/21.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/22.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --variant ${vcf}/X.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz \
                    --output ${i}.count.csv \
                    --output-format TABLE \
                    --min-base-quality 20 --disable-read-filter NotDuplicateReadFilter \
                    --reference /pine/scr/n/i/nil1/Homo_sapiens.GRCh38.dna.primary_assembly.fa \
                    --verbosity WARNING;


exit 0;
