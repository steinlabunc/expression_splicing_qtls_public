#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=40g
#SBATCH -t 2:00:00

module add plink;
module add gcta;
module add r/4.0.1;

i=$1
ty=$2


module add plink;
module add gcta;
module add r/4.0.1;

Rscript FUSION.post_processN.R \
        --sumstats ../sumstats/N.${ty}.sumstats.txt \
        --input ../conditional/R00.neuro.${ty}.top \
        --out ../conditional/R00.neuron.${ty}.${i}.top.analysis \
        --ref_ld_chr ../Genotype/R00/common_chr${i}_N \
        --chr ${i} \
        --locus_win 100000
        --plot --plot_individual --locus_win 100000 --report


