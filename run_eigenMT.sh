#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=30g
#SBATCH -t 35:00:00


module add python/3.6.6

#i=$1
i="X"
dir="/proj/steinlab/projects/R00/eQTLanalysis/multiple_test_correction/eigenMT/"

python3 ${dir}eigenMT.py --CHROM ${i} \
        --QTL qtls${i}.txt \
        --GEN genotypes${i}.txt \
        --GENPOS gen.positions${i}.txt \
        --PHEPOS pro.phe.positions.txt \
        --var_thresh 0.99 \
        --cis_dist 1000000 \
        --OUT chr${i}_N \
        --window 200
