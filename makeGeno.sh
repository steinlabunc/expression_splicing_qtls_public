#!/bin/bash
#SBATCH -p steinlab
#SBATCH -n 1
#SBATCH --mem=10g
#SBATCH -t 3:00:00

#Generate list of snps for specific chromosomes, and filter out snps with missing any of the subjects
module add plink;

for i in {1..22}
do

#i=$1
plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --freqx \
      --make-bed \
      --nonfounders \
      --keep ../pro.donors.txt \
      --out  ../genotype/chr${i}_Progenitor;


awk '$5 != 1 && $6 > 1 && $7 != 1 {print $2}' ../genotype/chr${i}_Progenitor.frqx > ../genotype/chr${i}_p_list.txt;
sed -i '1d' ../genotype/chr${i}_p_list.txt;

plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --freqx \
      --make-bed \
      --nonfounders \
      --keep ../neuro.donors.txt \
      --out ../genotype/chr${i}_Neuron;


awk '$5 != 1 && $6 > 1 && $7 != 1 {print $2}' ../genotype/chr${i}_Neuron.frqx > ../genotype/chr${i}_n_list.txt;
sed -i '1d' ../genotype/chr${i}_n_list.txt;


plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --keep ../pro.donors.txt \
      --extract ../genotype/chr${i}_p_list.txt \
      --make-bed \
      --recode12 \
      --output-missing-genotype 0 \
      --transpose \
      --out ../genotype/chr${i}_P.uniq


plink --bfile /proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/${i}.dose.R2g03.QC.IDfixed \
      --keep ../neuro.donors.txt \
      --extract ../genotype/chr${i}_n_list.txt \
      --make-bed \
      --recode12 \
      --output-missing-genotype 0 \
      --transpose \
      --out ../genotype/chr${i}_N.uniq
