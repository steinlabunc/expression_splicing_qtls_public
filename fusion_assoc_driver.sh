#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=50g
#SBATCH -t 3-00:00:00

module add plink;
module add gcta;
module add r/4.0.1;

i=$1
ty=$2
j=$3

Rscript FUSION.assoc_test.R \
--sumstats /proj/steinlab/projects/R00/eQTLanalysis/TWAS/sumstats/P.$ty.sumstats.txt \
--weights /proj/steinlab/projects/R00/eQTLanalysis/TWAS/sublist/prolist.$i.$j.txt \
--weights_dir /proj/steinlab/projects/R00/eQTLanalysis/TWAS/weights/cell/progenitor/ \
--ref_ld_chr /proj/steinlab/projects/R00/eQTLanalysis/TWAS/Genotype/R00/common_chr${i}_P \
--max_impute 0.40 \
--chr $i \
--out /pine/scr/n/i/nil1/TWAS/R00.Progenitor.$ty.$i.$j.dat


#Rscript FUSION.assoc_test.R \
#--sumstats /proj/steinlab/projects/R00/eQTLanalysis/TWAS/P.$ty.sumstats.txt \
#--weights /proj/steinlab/projects/R00/eQTLanalysis/TWAS/prolist.txt \
#--weights_dir /proj/steinlab/projects/R00/eQTLanalysis/TWAS/out/ \
#--ref_ld_chr /proj/steinlab/projects/R00/eQTLanalysis/TWAS/Genotype/EUR/new.common_EUR_chr${i}_P \
#--max_impute 0.65 \
#--chr $i \
#--out /proj/steinlab/projects/R00/eQTLanalysis/TWAS/EUR.Progenitor.$ty.$i.dat
